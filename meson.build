project('apertis-update-manager', 'c')

prefixdir = get_option('prefix')

gnome = import('gnome')

glib = dependency('glib-2.0')
gobject = dependency('gobject-2.0')
gio = dependency('gio-2.0 gio-unix-2.0')
ostree = dependency('ostree-1')
systemd = dependency('systemd')
udisks2 = dependency('udisks2')
zlib = dependency('zlib')

datadir = get_option('datadir')

systemunitdir = systemd.get_pkgconfig_variable('systemdsystemunitdir')
pkgdatadir = join_paths(datadir, meson.project_name())

# Can't be found in packageconf, explicitly use the system directory
dbuspolicydir = join_paths(datadir, 'dbus-1/system.d')

add_global_arguments('-DG_LOG_DOMAIN="AUM"', language : 'c')

managerd_sources = [ 'managerd/main.c',
                     'managerd/boot-state.c',
                     'managerd/boot-state.h',
                     'managerd/boot-state-file.c',
                     'managerd/boot-state-file.h',
                     'managerd/boot-state-uboot-env.c',
                     'managerd/boot-state-uboot-env.h',
                     'managerd/boot-state-uboot-ext.c',
                     'managerd/boot-state-uboot-ext.h',
                     'managerd/boot-state-efi.c',
                     'managerd/boot-state-efi.h',
                     'managerd/boot-state-nvmem.c',
                     'managerd/boot-state-nvmem.h',
                     'managerd/common.h',
                     'managerd/ostree-upgrader.c',
                     'managerd/ostree-upgrader.h' ]

managerd_gdbus = gnome.gdbus_codegen ('apertis-update-manager-dbus',
                                      'data/apertis-update-manager-dbus.xml',
                                      interface_prefix:
                                      'org.apertis',
                                      namespace: 'aum')
managerd_sources += managerd_gdbus

managerd_enums = gnome.mkenums('managerd-enums',
                               sources: ['managerd/ostree-upgrader.h'],
                               h_template: 'managerd-enums.h.in',
                               c_template: 'managerd-enums.c.in' )
managerd_sources += managerd_enums


executable('apertis-update-managerd',
           managerd_sources,
           install: true,
           install_dir: get_option('libexecdir'),
           dependencies: [ glib, gobject, gio, systemd, ostree, zlib, udisks2])

# updatectl tool
updatectl_sources = [ 'tools/updatectl.c' ]
updatectl_sources += managerd_gdbus

executable('updatectl',
            updatectl_sources,
            install: true,
            dependencies: [ glib, gobject, gio ])


envread_sources = [ 'tools/envread.c' ]
envread_sources += [ 'managerd/boot-state.c' ]
envread_sources += [ 'managerd/boot-state-file.c' ]
envread_sources += [ 'managerd/boot-state-uboot-env.c' ]
envread_sources += [ 'managerd/boot-state-uboot-ext.c' ]
envread_sources += [ 'managerd/boot-state-efi.c' ]
envread_sources += [ 'managerd/boot-state-nvmem.c' ]

executable('envread',
            envread_sources,
            install: true,
            dependencies: [ glib, gobject, gio, zlib ])

conf = configuration_data()
conf.set('libexecdir',
  join_paths(get_option('prefix'), get_option('libexecdir')))

conf.set('bindir',
  join_paths(get_option('prefix'), get_option('bindir')))

# configuration file for common definitions
configure_file(input: 'config.h.in',
                output: 'config.h',
                configuration: conf)

service_aum = configure_file(input: 'data/apertis-update-manager.service.in',
               output: 'apertis-update-manager.service',
               configuration: conf)

service_auc = configure_file(input: 'data/apertis-update-complete.service.in',
               output: 'apertis-update-complete.service',
               configuration: conf)

install_data(service_aum, service_auc,
             install_dir: systemunitdir)

install_data (['data/org.apertis.ApertisUpdateManager.conf' ],
              install_dir: dbuspolicydir)

install_data (['data/apertis-update-manager.ini' ],
              install_dir: '/etc')
