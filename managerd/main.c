/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */


#include "common.h"
#include "ostree-upgrader.h"
#include "boot-state.h"

#include "apertis-update-manager-dbus.h"

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <gio/gunixfdlist.h>
#include <systemd/sd-daemon.h>
#include <udisks/udisks.h>

#define POLL_SECONDS 300

#define DEFAULT_BOOTLIMIT 3
#define DECRYPT_KEYS_PATH "/usr/share/apertis-update-manager"

#define BUNDLE_NAME "static-update.bundle"
#define ENCRYPTED_BUNDLE_NAME BUNDLE_NAME ".enc"

typedef struct
{
  GNetworkMonitor *monitor;
  gboolean online;
  GMainLoop *loop;
  AumOstreeUpgrader *upgrader;
  gchar *boot_state_type;
  guint boot_limit;
  AumBootState *boot_state;
  GVolumeMonitor *volumes;

  GDBusConnection *bus;
  aumApertisUpdateManager *aum_dbus;

  GQueue upgrade_handlers;

  gboolean resetting_system;

  UDisksClient *udisks_client;
  gchar *loop_path;
  gchar *backing_file;
  gchar *encrypted_bundle_path;
  int encrypted_mount_pending;
  GQueue pending_mount;
} AUM;

typedef struct
{
  AUM *aum;
  gchar *name;
  guint watch_id;
} UpgradeHandler;

static void
cleanup_encrypted_bundle(AUM *aum, UDisksObject *object);

static void
handler_vanished (GDBusConnection *connection,
                  const gchar *name,
                  gpointer user_data)
{
  UpgradeHandler *handler = user_data;

  g_queue_remove (&handler->aum->upgrade_handlers, handler);

  g_bus_unwatch_name (handler->watch_id);
  g_free (handler->name);
  g_slice_free (UpgradeHandler, handler);
}

static void
aum_add_upgrade_handler (AUM *aum, const gchar *name)
{
  UpgradeHandler *handler = g_slice_new0 (UpgradeHandler);

  handler->aum = aum;
  handler->name = g_strdup (name);
  handler->watch_id = g_bus_watch_name_on_connection (aum->bus,
                                                      handler->name,
                                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                                      NULL,
                                                      handler_vanished,
                                                      handler,
                                                      NULL);

  g_queue_push_head (&aum->upgrade_handlers, handler);
}

static void
aum_update_network_status (AUM * aum)
{
  gboolean active;
  gboolean online = g_network_monitor_get_network_available (aum->monitor);
  guint polling_time;

  /* GNetworkMonitor seems to signal changes somewhat spuriously so filter out
   * the redundant ones */
  if (online == aum->online)
    return;

  g_message ("Network status: %s", online ? "online" : "offline");

  aum->online = online;
  g_object_set (aum->aum_dbus, "network-connected", online, NULL);
  g_object_get (aum->aum_dbus, "updates-from-network", &active, NULL);
  g_object_get (aum->aum_dbus, "updates-polling-time", &polling_time, NULL);

  aum_ostree_upgrader_set_active (aum->upgrader, online && active, polling_time);
}

static void
upgrade_state_changed (AumOstreeUpgrader *upgrader,
                       GParamSpec *pspec,
                       gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (UDisksObject) loop_object = NULL;
  AumOstreeUpgradeState state;

  g_object_get (upgrader, "upgrade-state", &state, NULL);

  if ((state == AUM_OSTREE_UPGRADE_STATE_PENDING || state == AUM_OSTREE_UPGRADE_STATE_UPTODATE)
      && aum->loop_path)
    {
      loop_object = udisks_client_get_object (aum->udisks_client, aum->loop_path);
      if (loop_object)
        cleanup_encrypted_bundle (aum, loop_object);
      else
        g_warning ("No encrypted bundle to cleanup for %s", aum->loop_path);
    }

  if (state == AUM_OSTREE_UPGRADE_STATE_PENDING)
    {
      aum_boot_state_set_update_available (aum->boot_state, TRUE);

      if (!aum->resetting_system
          && g_queue_get_length (&aum->upgrade_handlers) == 0)
        {
          aum_ostree_upgrade_apply_pending (upgrader);
        }
      else
        {
          g_message ("Upgrade handlers registered or the system is being reset, not rebooting");
        }
    }
}

static void
network_monitor_changed (GNetworkMonitor * monitor,
                         gboolean available, gpointer user_data)
{
  AUM *aum = user_data;

  aum_update_network_status (aum);
}

static gboolean
_pending_cleanup_bundle_cb(gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (UDisksObject) loop_object = NULL;

  loop_object = udisks_client_get_object (aum->udisks_client, aum->loop_path);
  if (loop_object)
    cleanup_encrypted_bundle (aum, loop_object);
  else
    g_warning ("Unable to retrieve loopback object for %s", aum->loop_path);

  return FALSE;
}

static void
cleanup_encrypted_bundle(AUM *aum, UDisksObject *object)
{
  UDisksEncrypted *encrypted;
  GError *error = NULL;

  encrypted = udisks_object_peek_encrypted (object);
  if (encrypted)
    {
      g_autoptr (UDisksObject) object;
      UDisksFilesystem *filesystem;
      GVariantBuilder options_builder;

      GVariant *var = g_dbus_proxy_get_cached_property (G_DBUS_PROXY (encrypted), "CleartextDevice");
      object = udisks_client_get_object (aum->udisks_client, g_variant_get_string(var, NULL));
      g_variant_unref (var);

      filesystem = udisks_object_peek_filesystem (object);
      if (filesystem)
        {
          const gchar *const *mountpoints = udisks_filesystem_get_mount_points (filesystem);

          /* Check if filesystem is mounted before trying to unmount it */
          if (!mountpoints || !mountpoints[0])
            goto lock_encrypted;

          if (aum->upgrader && aum_ostree_upgrader_static_delta_is_running (aum->upgrader))
            {
              AumOstreeUpgradeState state;

              g_object_get (aum->upgrader, "upgrade-state", &state, NULL);

              if (state == AUM_OSTREE_UPGRADE_STATE_PENDING || state == AUM_OSTREE_UPGRADE_STATE_UPTODATE)
                {
                  g_debug ("Waiting for static delta upgrade to finish");
                }
              else
                {
                  aum_ostree_upgrader_static_delta_cancel (aum->upgrader);
                  g_debug ("Waiting for static delta upgrade to be cancelled");
                }
              g_timeout_add_seconds (1, _pending_cleanup_bundle_cb, aum);
              return;
            }

          g_variant_builder_init (&options_builder, G_VARIANT_TYPE_VARDICT);
          g_variant_builder_add (&options_builder, "{sv}", "auth.no_user_interaction", g_variant_new_boolean (TRUE));
          g_variant_builder_add (&options_builder, "{sv}", "force", g_variant_new_boolean (TRUE));
          if (!udisks_filesystem_call_unmount_sync (filesystem,
                                                    g_variant_builder_end (&options_builder),
                                                    NULL,
                                                    &error))
            {
              g_message ("Error unmounting encrypted filesystem: %s",
                         error ? error->message : NULL);
              g_clear_error (&error);
            }
          udisks_client_settle (aum->udisks_client);
        }

lock_encrypted:
      g_variant_builder_init (&options_builder, G_VARIANT_TYPE_VARDICT);
      g_variant_builder_add (&options_builder, "{sv}", "auth.no_user_interaction", g_variant_new_boolean (TRUE));
      if (!udisks_encrypted_call_lock_sync (encrypted,
                                            g_variant_builder_end (&options_builder),
                                            NULL,
                                            &error))
        {
          g_message ("Error locking encrypted filesystem: %s",
                     error ? error->message : NULL);
          g_clear_error (&error);
        }
    }

  g_clear_pointer (&aum->loop_path, g_free);
  g_clear_pointer (&aum->backing_file, g_free);
  g_clear_pointer (&aum->encrypted_bundle_path, g_free);
}

static void
cleanup_encrypted_bundle_loop_devices (AUM *aum)
{
  GVariantBuilder options_builder;
  g_autoptr (GVariant) options;
  gchar **devices;
  int i;
  GError *error = NULL;

  g_variant_builder_init (&options_builder, G_VARIANT_TYPE_VARDICT);
  g_variant_builder_add (&options_builder, "{sv}", "auth.no_user_interaction", g_variant_new_boolean (TRUE));
  options = g_variant_builder_end (&options_builder);
  g_variant_ref_sink (options);

  /* List all block devices */
  if (!udisks_manager_call_get_block_devices_sync (udisks_client_get_manager (aum->udisks_client),
                                                   options,
                                                   &devices,
                                                   NULL,
                                                   &error))
    {
      g_message ("Error getting block devices: %s", error ? error->message : NULL);
      g_clear_error (&error);
      return;
    }

  for (i = 0; devices[i]; i++)
    {
      g_autoptr (UDisksObject) object = NULL;
      g_autoptr (UDisksLoop) loop = NULL;

      /* if the block device is a loop device and its backing file is an
         encrypted bundle, assume it's a leftover from a previous run and
         should be cleaned up */
      object = udisks_client_get_object (aum->udisks_client, devices[i]);
      loop = udisks_object_get_loop (object);
      if (loop && g_str_has_suffix (udisks_loop_get_backing_file (loop), ENCRYPTED_BUNDLE_NAME))
        {
          gboolean autoclear = udisks_loop_get_autoclear (loop);
          g_debug ("Found static delta upgrade %s for loop device %s during start, removing it",
                   udisks_loop_get_backing_file (loop),
                   udisks_block_get_device (udisks_object_peek_block (object)));
          cleanup_encrypted_bundle (aum, object);
          if (!autoclear && !udisks_loop_call_delete_sync (loop, options, NULL, &error))
            {
              g_message ("Error deleting loop device %s: %s",
                         udisks_block_get_device (udisks_object_peek_block (object)),
                         error ? error->message : NULL);
              g_clear_error (&error);
            }
        }
    }

  g_strfreev (devices);
}

static void
udisks2_properties_changed (GDBusObjectManagerClient *manager,
                            GDBusObjectProxy         *object_proxy,
                            GDBusProxy               *interface_proxy,
                            GVariant                 *changed_properties,
                            const gchar* const       *invalidated_properties,
                            gpointer                  user_data)
{
  AUM *aum = user_data;
  GVariantIter *iter;
  const gchar *property_name;
  GVariant *value;

  if (g_strcmp0 (g_dbus_object_get_object_path (G_DBUS_OBJECT (object_proxy)),
                 aum->loop_path) != 0 ||
      g_strcmp0 (g_dbus_proxy_get_interface_name (interface_proxy),
                 "org.freedesktop.UDisks2.Loop") != 0 ||
      !aum->backing_file)
    return;

  g_variant_get (changed_properties, "a{sv}", &iter);
  while (g_variant_iter_next (iter, "{&sv}", &property_name, &value))
    {
      if (g_strcmp0 (property_name, "BackingFile") != 0)
        continue;

      if (g_strcmp0 (g_variant_get_bytestring (value), aum->backing_file) == 0)
        return;

      cleanup_encrypted_bundle (aum, UDISKS_OBJECT (object_proxy));
      break;
    }
}

typedef struct {
  AUM *aum;
  gchar *filename;
} unlock_and_mount_encrypted_data;

static void
unlock_and_mount_encrypted_data_free (gpointer ptr)
{
  unlock_and_mount_encrypted_data *data = ptr;
  g_free (data->filename);
  g_free (data);
}

void
unlock_and_mount_encrypted_thread (GTask * task,
                                   gpointer source,
                                   gpointer task_data,
                                   GCancellable *cancellable)
{
  unlock_and_mount_encrypted_data *data = task_data;
  AUM *aum = data->aum;
  gchar *filename = data->filename;
  int fd;
  g_autoptr (GUnixFDList) fd_list = NULL;
  GVariantBuilder options_builder;
  g_autoptr (GVariant) options = NULL;
  gchar *resulting_object_path = NULL;
  g_autoptr (UDisksObject) loop_object = NULL;
  g_autoptr (UDisksObject) mount_object = NULL;
  g_autoptr (GDir) dir = NULL;
  UDisksFilesystem *filesystem;
  GError *error = NULL;

  if (aum->encrypted_bundle_path)
    {
      g_debug ("Encrypted bundle already mounted");
      return;
    }

  /* Create loop device */
  fd = open (filename, O_RDONLY);
  if (fd == -1)
    {
      g_message ("Error opening file %s: %s", filename, g_strerror (errno));
      return;
    }
  fd_list = g_unix_fd_list_new_from_array (&fd, 1);
  aum->backing_file = g_strdup(filename);

  g_variant_builder_init (&options_builder, G_VARIANT_TYPE_VARDICT);
  g_variant_builder_add (&options_builder, "{sv}", "auth.no_user_interaction", g_variant_new_boolean (TRUE));
  g_variant_builder_add (&options_builder, "{sv}", "read-only", g_variant_new_boolean (TRUE));
  if (!udisks_manager_call_loop_setup_sync (udisks_client_get_manager (aum->udisks_client),
                                            g_variant_new_handle (0),
                                            g_variant_builder_end (&options_builder),
                                            fd_list,
                                            &aum->loop_path,
                                            NULL,
                                            NULL,
                                            &error))
    {
      g_clear_pointer (&aum->backing_file, g_free);
      g_message ("Error setting up loop device for %s: %s",
                 filename, error ? error->message : NULL);
      g_clear_error (&error);
      return;
    }
  udisks_client_settle (aum->udisks_client);

  loop_object = udisks_client_get_object (aum->udisks_client, aum->loop_path);

  g_variant_builder_init (&options_builder, G_VARIANT_TYPE_VARDICT);
  g_variant_builder_add (&options_builder, "{sv}", "auth.no_user_interaction", g_variant_new_boolean (TRUE));
  options = g_variant_builder_end (&options_builder);
  g_variant_ref_sink (options);

  /* Encrypted interface can take times to appears in the loop object, mainly
   * just after boot
   * re-tries for 2 seconds, if it still fails this may be because there's
   * no encrypted data in the file */
  int i = 20;
  while (i--)
    {
      if (udisks_object_peek_encrypted (loop_object))
        break;
      g_usleep (0.1 * G_USEC_PER_SEC);
      udisks_client_settle (aum->udisks_client);
      g_debug ("retrying");
    }
  if (!i)
    {
      g_message ("No encrypted data found in %s", filename);

      if (!udisks_loop_call_delete_sync (udisks_object_peek_loop (loop_object),
                                         options,
                                         NULL,
                                         &error))
        {
          g_message ("Error deleting loop device %s: %s",
                     udisks_block_get_device (udisks_object_peek_block (loop_object)),
                     error ? error->message : NULL);
          g_clear_error (&error);
        }
      g_clear_pointer (&aum->loop_path, g_free);

      return;
    }

  /* Unlock the filesystem */
  dir = g_dir_open (DECRYPT_KEYS_PATH, 0, &error);
  if (dir == NULL)
    {
      g_message ("Failed to open keys directory: %s", error ? error->message : NULL);
      g_clear_error (&error);
      goto unlock_failed;
    }
  for (const gchar *n = g_dir_read_name (dir); n != NULL; n = g_dir_read_name (dir))
    {
      g_autofree gchar *p = g_build_filename (DECRYPT_KEYS_PATH, n, NULL);
      struct stat st;
      g_autofree gchar *passphrase = NULL;

      g_debug ("Trying key from %s", p);

      if (stat (p, &st) != 0)
        {
          g_message ("Could not read key file %s: %s", p, g_strerror (errno));
          continue;
        }
      if (st.st_uid != 0)
        {
          g_message ("Could not read key file %s: file must be owned by root", p);
          continue;
        }
      if (st.st_mode & (S_IRWXG | S_IRWXO | S_ISUID))
        {
          g_message ("Could not read key file %s: invalid file permissions", p);
          continue;
        }

      if (!g_file_get_contents (p, &passphrase, NULL, &error))
        {
          g_message ("Error reading passphrase: %s", error->message);
          continue;
        }

      if (udisks_encrypted_call_unlock_sync (udisks_object_peek_encrypted (loop_object),
                                             passphrase,
                                             options,
                                             &resulting_object_path,
                                             NULL,
                                             NULL))
        break;
    }
  if (!resulting_object_path)
    {
      g_message ("Unable to unlock %s", udisks_block_get_device (udisks_object_peek_block (loop_object)));

unlock_failed:
      if (!udisks_loop_call_delete_sync (udisks_object_peek_loop (loop_object),
                                         options,
                                         NULL,
                                         &error))
        {
          g_message ("Error deleting loop device %s: %s",
                     udisks_block_get_device (udisks_object_peek_block (loop_object)),
                     error ? error->message : NULL);
          g_clear_error (&error);
        }
      g_clear_pointer (&aum->loop_path, g_free);

      set_system_upgrade_error(aum->aum_dbus, AUM_ERROR_UNABLE_TO_UNLOCK,
		               "Unable to unlock bundle file");
      return;
    }
  udisks_client_settle (aum->udisks_client);

  /* Now that filesystem is unlocked, we can set loop device to autoclear,
   * this allows loop device to be automatically removed when filesystem
   * will be lock */
  if (!udisks_loop_call_set_autoclear_sync (udisks_object_peek_loop (loop_object),
                                            TRUE,
                                            g_variant_new ("a{sv}", NULL),
                                            NULL,
                                            &error))
    {
      g_message ("Error setting autoclear loop device for %s: %s",
                 filename, error ? error->message : NULL);
      g_clear_error (&error);
      return;
    }

  /* Retrieve filesystem */
  mount_object = udisks_client_get_object (aum->udisks_client, resulting_object_path);
  g_free (resulting_object_path);
  filesystem = udisks_object_peek_filesystem (mount_object);
  if (filesystem == NULL)
    {
      g_message ("%s is not a mountable filesystem.", filename);
      cleanup_encrypted_bundle (aum, loop_object);
      return;
    }

  /* Mount partition */
  if (!udisks_filesystem_call_mount_sync (filesystem,
                                          options,
                                          &aum->encrypted_bundle_path,
                                          NULL,
                                          &error))
    {
      g_message ("Error mounting encrypted filesystem: %s",
                 error ? error->message : NULL);
      cleanup_encrypted_bundle (aum, loop_object);
      g_clear_error (&error);
      return;
    }
}

static void process_mount (AUM *aum, gchar *path);

static void
unlock_and_mount_encrypted_cb (GObject * object,
                               GAsyncResult * res,
                               gpointer user_data)
{
  AUM *aum = user_data;
  gchar *path;

  if (aum->encrypted_mount_pending > 0)
    aum->encrypted_mount_pending--;
  else
    g_warning ("Bad count of encrypted mount pending");

  while (!aum->encrypted_mount_pending &&
         !g_queue_is_empty (&aum->pending_mount))
  {
    path = g_queue_pop_head (&aum->pending_mount);
    g_message ("processing mount : %s", path);
    process_mount (aum, path);
    g_free(path);
  }
}

static void
process_mount (AUM *aum, gchar *path)
{
  g_autofree gchar *filename = NULL;
  unlock_and_mount_encrypted_data *data = NULL;
  GTask *task = NULL;

  if (!g_file_test (DECRYPT_KEYS_PATH, G_FILE_TEST_IS_DIR) ||
      g_strcmp0(path, aum->encrypted_bundle_path) == 0)
    {
      /* TODO: clarify future naming, pattern matching? */
      filename = g_build_filename (path, BUNDLE_NAME, NULL);

      if (!g_file_test (filename, G_FILE_TEST_EXISTS))
        {
          g_message ("No " BUNDLE_NAME " found");
          return;
        }

      g_debug ("Upgrading using %s", filename);
      aum_ostree_upgrade_apply_static_delta (aum->upgrader, filename);

      return;
    }

  filename = g_build_filename (path, ENCRYPTED_BUNDLE_NAME, NULL);
  if (!g_file_test (filename, G_FILE_TEST_EXISTS))
    {
      g_free (filename);
      filename = g_build_filename (path, BUNDLE_NAME, NULL);
      if (g_file_test (filename, G_FILE_TEST_EXISTS))
        g_warning ("Trying to use unencrypted bundle instead of encrypted bundle");

      g_message ("No " ENCRYPTED_BUNDLE_NAME " found");
      return;
    }

  /* Start unlocking and mount encrypted bundle in its own thread as unlocking
     may take time and prevent other DBus method to reply in time */
  aum->encrypted_mount_pending++;
  aum_ostree_upgrade_set_state(aum->upgrader, AUM_OSTREE_UPGRADE_STATE_DECRYPTING);
  data = g_new0 (unlock_and_mount_encrypted_data, 1);
  data->aum = aum;
  data->filename = g_strdup (filename);
  task = g_task_new (NULL, NULL, unlock_and_mount_encrypted_cb, aum);
  g_task_set_task_data (task, data, unlock_and_mount_encrypted_data_free);
  g_task_run_in_thread (task, unlock_and_mount_encrypted_thread);
  g_object_unref (task);
}

static void
mount_added (GVolumeMonitor * volumem,
             GMount *mount, gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GFile) location = NULL;
  g_autofree gchar *path = NULL;

  if (aum->resetting_system)
    return;

  location = g_mount_get_default_location (mount);
  path = g_file_get_path (location);

  if (aum->encrypted_mount_pending)
  {
    g_message ("mount defered : %s", path);
    g_queue_push_tail (&aum->pending_mount, g_strdup(path));
    return;
  }

  g_message ("mount added : %s", path);
  process_mount(aum, path);
}

static gboolean
setup_runtime_dir(void)
{

  if (!g_file_test (AUM_RUNTIME_DIR, G_FILE_TEST_IS_DIR))
    {
      int ret;
      ret = g_mkdir(AUM_RUNTIME_DIR, 0700);

      if (ret != 0)
        {
          g_message ("Couldn't create runtime directory, Exiting...");
          return FALSE;
        }
    }
  else
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (GDir) dir = NULL;

      dir = g_dir_open (AUM_RUNTIME_DIR, 0, &error);
      if (dir == NULL)
        {
          g_message ("Failed to open runtime directory, Exiting...");
          return FALSE;
        }
      for (const gchar *n = g_dir_read_name (dir);
           n != NULL;
           n = g_dir_read_name (dir))
        {
          g_autofree gchar *p = g_build_filename (AUM_RUNTIME_DIR, n, NULL);
          g_unlink (p);
        }
    }

  return TRUE;
}

static gboolean
handle_register_system_upgrade_handler (aumApertisUpdateManager *dbus,
                                        GDBusMethodInvocation *invocation,
                                        gpointer user_data)
{
  AUM *aum = user_data;

  aum_add_upgrade_handler (aum,
                           g_dbus_method_invocation_get_sender (invocation));

  g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_apply_system_upgrade (aumApertisUpdateManager *dbus,
                             GDBusMethodInvocation *invocation,
                             gpointer user_data)
{
  AUM *aum = user_data;

  if (!aum->resetting_system)
    {
      aum_ostree_upgrade_apply_pending (aum->upgrader);
    }
  g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
mark_update_successful(AUM *aum)
{
  gboolean result = FALSE;

  if (!aum_boot_state_get_update_available (aum->boot_state))
    return TRUE;

  if (boot_state_check_boot_successful(aum->boot_state))
    {
      g_message ("Marked update as successful");
      result = aum_ostree_upgrader_set_refs(aum->upgrader);
    }
  else
    {
      g_message ("Undeploying");
      result = aum_ostree_upgrader_undeploy (aum->upgrader);
    }

  if (result)
    result = aum_boot_state_set_update_available (aum->boot_state, FALSE);

  return result;
}

static gboolean
read_configuration_file (AUM *aum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();
  gboolean updates_from_network = FALSE;
  guint polling_time = POLL_SECONDS;

  aum->boot_state_type = NULL;
  aum->boot_limit = DEFAULT_BOOTLIMIT;

  key_file = g_key_file_new ();
  if (!g_key_file_load_from_file (key_file, "/etc/apertis-update-manager.ini", G_KEY_FILE_NONE, &error))
    goto err;

  if (g_key_file_has_key (key_file, "Manager", "UpdatesFromNetwork", NULL))
    updates_from_network = g_key_file_get_boolean (key_file,
                                                   "Manager", "UpdatesFromNetwork",
                                                   NULL);

  if (g_key_file_has_key (key_file, "Manager", "UpdatesPollingTime", NULL))
    polling_time = (guint) g_key_file_get_integer (key_file,
                                                   "Manager", "UpdatesPollingTime",
                                                   NULL);

  if (g_key_file_has_key (key_file, "Manager", "BootState", NULL))
    aum->boot_state_type = g_key_file_get_string (key_file,
                                                  "Manager", "BootState",
                                                  NULL);

  if (g_key_file_has_key (key_file, "Manager", "BootLimit", NULL))
    aum->boot_limit = g_key_file_get_integer (key_file,
                                              "Manager", "BootLimit",
                                              NULL);

  g_object_set (aum->aum_dbus, "updates-from-network", updates_from_network, NULL);
  g_object_set (aum->aum_dbus, "updates-polling-time", polling_time, NULL);

  return TRUE;

err:
  g_warning ("Could not load configuration file: %s", error->message);
  return FALSE;
}

static gboolean
handle_mark_update_successful (aumApertisUpdateManager *dbus,
                             GDBusMethodInvocation *invocation,
                             gpointer user_data)
{
  AUM *aum = user_data;

  if (aum->resetting_system)
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "System is being reset");
  else if (!mark_update_successful (aum))
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "Failed to mark update successful");
  else
    g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_apply_static_delta (aumApertisUpdateManager *dbus,
                           GDBusMethodInvocation *invocation,
                           gchar *filename,
                           gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GError) error = NULL;

  if (aum->resetting_system)
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "System is being reset");
  else if (!aum_ostree_upgrade_apply_static_delta (aum->upgrader, filename))
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "Failed to apply static delta");
  else
    g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_check_network_updates (aumApertisUpdateManager *dbus,
                              GDBusMethodInvocation *invocation,
                              gpointer user_data)
{
  AUM *aum = user_data;
  guint polling_time;

  if (aum->resetting_system)
    {
      g_dbus_method_invocation_return_dbus_error (invocation,
                                                  "org.apertis.ApertisUpdateManager",
                                                  "System is being reset");
      goto out;
    }

  /* Set the property to TRUE allowing online updates */
  aum_apertis_update_manager_set_updates_from_network (aum->aum_dbus, TRUE);

  /* Start server(s) polling */
  aum_ostree_upgrader_set_ready (aum->upgrader);
  g_object_get (dbus, "updates-polling-time", &polling_time, NULL);
  aum_ostree_upgrader_set_active (aum->upgrader, aum->online, polling_time);

  g_dbus_method_invocation_return_value (invocation, NULL);

out:
  return TRUE;
}

static gboolean
handle_dry_run_mode (aumApertisUpdateManager *dbus,
                     GDBusMethodInvocation *invocation,
                     gboolean mode,
                     gpointer user_data)
{
  AUM *aum = user_data;

  if (aum->resetting_system)
    {
      g_dbus_method_invocation_return_dbus_error (invocation,
                                                  "org.apertis.ApertisUpdateManager",
                                                  "System is being reset");
      goto out;
    }

  g_debug ("Dry run mode: %s", mode ? "enabled" : "disabled");
  aum_ostree_upgrader_set_dry_run (aum->upgrader, mode);

  g_dbus_method_invocation_return_value (invocation, NULL);

out:
  return TRUE;
}

static void
updates_from_network_changed (aumApertisUpdateManager *dbus,
                              GParamSpec *pspec,
                              gpointer user_data)
{
  AUM *aum = user_data;
  gboolean active, online;
  guint polling_time;

  g_object_get (dbus, "updates-from-network", &active, NULL);
  g_object_get (dbus, "network-connected", &online, NULL);
  g_object_get (dbus, "updates-polling-time", &polling_time, NULL);

  g_message ("Auto update status: %s", active ? "active" : "disabled");

  aum_ostree_upgrader_set_active (aum->upgrader, online && active, polling_time);
}

static void
updates_polling_time_changed (aumApertisUpdateManager *dbus,
                              GParamSpec *pspec,
                              gpointer user_data)
{
  AUM *aum = user_data;
  gboolean active, online;
  guint polling_time;

  g_object_get (dbus, "updates-polling-time", &polling_time, NULL);
  g_message ("Set polling time for OTA upgrades: %u", polling_time);

  /* Reset timer */
  aum_ostree_upgrader_set_active (aum->upgrader, FALSE, polling_time);

  g_object_get (dbus, "updates-from-network", &active, NULL);
  g_object_get (dbus, "network-connected", &online, NULL);

  aum_ostree_upgrader_set_active (aum->upgrader, online && active, polling_time);
}

static void
bus_acquired_callback (GDBusConnection *connection,
                       const gchar *name,
                       gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GError) error = NULL;
  gboolean active;

  aum->bus = g_object_ref (connection);
  aum->aum_dbus = aum_apertis_update_manager_skeleton_new ();

  g_signal_connect (aum->aum_dbus, "handle-register-system-upgrade-handler",
                    G_CALLBACK (handle_register_system_upgrade_handler),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-apply-system-upgrade",
                    G_CALLBACK (handle_apply_system_upgrade),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-mark-update-successful",
                    G_CALLBACK (handle_mark_update_successful),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-apply-static-delta",
                    G_CALLBACK (handle_apply_static_delta),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-check-network-updates",
                    G_CALLBACK (handle_check_network_updates),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-dry-run-mode",
                    G_CALLBACK (handle_dry_run_mode),
                    aum);

  read_configuration_file (aum);

  g_object_get (aum->aum_dbus, "updates-from-network", &active, NULL);
  g_message ("Auto update status: %s", active ? "active" : "disabled");
  g_signal_connect (aum->aum_dbus, "notify::updates-from-network",
                    G_CALLBACK (updates_from_network_changed),
                    aum);

  g_signal_connect (aum->aum_dbus, "notify::updates-polling-time",
                    G_CALLBACK (updates_polling_time_changed),
                    aum);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (aum->aum_dbus),
                                         connection, "/", &error))
    {
      g_warning ("Failed to export dbus interface: %s", error->message);
      g_main_loop_quit (aum->loop);
    }
}

static void
name_acquired_callback (GDBusConnection *connection,
                        const gchar *name,
                        gpointer user_data)
{
  AUM *aum = user_data;

  /* Fully setup lets go to work */
  aum->monitor = g_network_monitor_get_default ();
  aum->upgrader = g_object_new (AUM_TYPE_OSTREE_UPGRADER, NULL);

  cleanup_encrypted_bundle_loop_devices (aum);

  if (aum->boot_state_type) {
    aum->boot_state = boot_state_factory_get_by_type(aum->boot_state_type);
  } else {
    aum->boot_state = boot_state_factory_get_active();
  }

  aum_boot_state_set_boot_limit (aum->boot_state, aum->boot_limit);
  set_system_upgrade_error(aum->aum_dbus, AUM_NO_ERROR, "No Error");

  aum->volumes = g_volume_monitor_get();

  g_signal_connect (aum->upgrader, "notify::upgrade-state",
                    G_CALLBACK (upgrade_state_changed), aum);

  g_object_bind_property (aum->upgrader, "upgrade-state",
                          aum->aum_dbus, "system-upgrade-state",
                          G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE);

  g_signal_connect (aum->monitor, "network-changed",
                    G_CALLBACK (network_monitor_changed), aum);

  g_signal_connect (aum->volumes, "mount-added",
                    G_CALLBACK (mount_added), aum);

  g_signal_connect (udisks_client_get_object_manager (aum->udisks_client),
                    "interface-proxy-properties-changed",
                    G_CALLBACK (udisks2_properties_changed),
                    aum);


  aum_update_network_status (aum);
}

static void
name_lost_handler (GDBusConnection *connection,
                   const gchar *name,
                   gpointer user_data)
{
  AUM *aum = user_data;

  g_warning ("Failed to get bus name %s", name);
  g_main_loop_quit (aum->loop);
}

int
main (int argc, char **argv)
{
  AUM *aum;
  g_autoptr (GError) error = NULL;

  if (!setup_runtime_dir())
    return -1;

  aum = g_new0 (AUM, 1);
  g_queue_init (&aum->pending_mount);

  aum->loop = g_main_loop_new (NULL, FALSE);
  g_bus_own_name (G_BUS_TYPE_SYSTEM,
                  "org.apertis.ApertisUpdateManager",
                  G_BUS_NAME_OWNER_FLAGS_NONE,
                  bus_acquired_callback,
                  name_acquired_callback,
                  name_lost_handler,
                  aum,
                  NULL);

  aum->udisks_client = udisks_client_new_sync (NULL, &error);
  if (aum->udisks_client == NULL)
    {
      g_message ("Error connecting to the udisks daemon: %s\n", error->message);
      g_clear_error (&error);
      goto out;
    }


  g_main_loop_run (aum->loop);

  if (aum->monitor)
    g_object_unref (aum->monitor);

  if (aum->volumes)
    g_object_unref (aum->volumes);

  if (aum->upgrader)
    g_object_unref (aum->upgrader);

  if (aum->boot_state)
    g_object_unref (aum->boot_state);

  if (aum->aum_dbus)
    g_object_unref (aum->aum_dbus);

  if (aum->bus)
    g_object_unref (aum->bus);

  g_clear_pointer (&aum->boot_state_type, g_free);

  g_object_unref (aum->udisks_client);
out:
  g_main_loop_unref (aum->loop);
  g_free (aum);

  return 0;
}
