/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 * Copyright © 2020 Robert Bosch Power Tools GmbH
 *
 * Derived from boot-state-file.c and libwatling.c
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstatenvmem
 * @title: AumBootStateNvmem
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include <gio/gio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include "boot-state-nvmem.h"

#define NVMEM_FILE "/sys/bus/nvmem/devices/omap_rtc_scratch0/nvmem"
#define CLEAR_COUNT 0x00
#define VERSION_1 0x01

/* RTC_SCRATCH_SIZE = NUMBER OF SCRATCH REGS * LENGTH OF SCRACTH REG */
/* RTC_SCRATCH_SIZE =           3            *        4 Bytes        */
#define RTC_SCRATCH_SIZE 12

enum {
  SCRATCH_REG2_BYTE0 = 0,
  SCRATCH_REG2_BYTE1,
  SCRATCH_REG2_BYTE2,
  SCRATCH_REG2_BYTE3,
  SCRATCH_REG_LEN
};

struct _AumBootStateNvmem
{
  GObject parent;
  const gchar *path;
  gint active;
  gint count;
  gint limit;
  gboolean update_available;
  guchar version;
  guchar magic;
};

static void
aum_boot_state_nvmem_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateNvmem, aum_boot_state_nvmem, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_nvmem_iface_init));

static gint
nvmem_write_scratch_reg(AumBootStateNvmem * self)
{
  gint fd;
  gint ret;
  guchar write_buf[SCRATCH_REG_LEN];

  if(self == NULL) {
    goto err;
  }

  fd = open(NVMEM_FILE, O_WRONLY);
  if (fd < 0)
    return -errno;

  /*    Scratch0 + Scratch1 + Scratch2   */
  /*      0..3   +   4..7   +   8..11    */
  /*  Seeking to Scratch2 i.e; 12-4 = 8  */
  ret = lseek(fd, RTC_SCRATCH_SIZE - SCRATCH_REG_LEN, SEEK_SET);
  if (ret < 0) {
    ret = -errno;
    goto err;
  }

  /* Update scratch register */
  write_buf[SCRATCH_REG2_BYTE0] = self->count & 0xFF ;

  if (self->version == VERSION_1)
    write_buf[SCRATCH_REG2_BYTE1] = CLEAR_COUNT;
  else
    write_buf[SCRATCH_REG2_BYTE1] = self->update_available;

  write_buf[SCRATCH_REG2_BYTE2] = self->version;
  write_buf[SCRATCH_REG2_BYTE3] = self->magic;

  ret = write(fd, write_buf, SCRATCH_REG_LEN);
  if (ret < 0)
    ret = -errno;

err:
  if (close(fd) < 0)
    g_message("Failed to close %s", NVMEM_FILE);

  return ret;
}

static gint
nvmem_read_scratch_reg(guchar *read_buf)
{
  gint fd;
  gint ret;

  if (!read_buf)
    return -EINVAL;

  fd = open(NVMEM_FILE, O_RDONLY);
  if (fd < 0)
    return -errno;

  /*    Scratch0 + Scratch1 + Scratch2   */
  /*      0..3   +   4..7   +   8..11    */
  /*  Seeking to Scratch2 i.e; 12-4 = 8  */
  ret = lseek(fd, RTC_SCRATCH_SIZE - SCRATCH_REG_LEN, SEEK_SET);
  if (ret < 0) {
    ret = -errno;
    goto err;
  }

  ret = read(fd, read_buf, SCRATCH_REG_LEN);
  if (ret < 0) {
    ret = -errno;
    goto err;
  }

err:
  if (close(fd) < 0)
    g_message("Failed to close %s", NVMEM_FILE);

  return ret;
}

static gboolean
aum_boot_state_nvmem_is_active (AumBootState * boot_state)
{
  AumBootStateNvmem *self = AUM_BOOT_STATE_NVMEM (boot_state);
  return self->active;
}

static const gchar *
aum_boot_state_nvmem_get_name (AumBootState * boot_state)
{
  return "nvmem";
}

static gboolean
aum_boot_state_nvmem_update_on_disk (AumBootStateNvmem * self)
{
  gint ret;

  ret = nvmem_write_scratch_reg(self);
  if(ret < 0){
    g_warning("[%s] Failed: update scratch reg", __func__);
    return FALSE;
  }

  return TRUE;
}

static gint
aum_boot_state_nvmem_get_boot_count (AumBootState * boot_state)
{
  AumBootStateNvmem *self = AUM_BOOT_STATE_NVMEM (boot_state);
  return self->count;
}

static gint
aum_boot_state_nvmem_get_boot_limit (AumBootState * boot_state)
{
  AumBootStateNvmem *self = AUM_BOOT_STATE_NVMEM (boot_state);
  return self->limit;
}

static void
aum_boot_state_nvmem_set_boot_limit (AumBootState * boot_state, guint limit)
{
  /* nvmem backend doesn't have support to set boot limit */
  /* This function is written to avoid printing the following message on journal */
  /* aum_boot_state_set_boot_limit: assertion 'AUM_BOOT_STATE_GET_IFACE (self)->set_boot_limit != NULL' failed */
}

static gboolean
aum_boot_state_nvmem_get_update_available (AumBootState * boot_state)
{
  /* nvmem backend store the upgrade status in RTC scratch register */
  /* which is volatile in nature, if power is interrupted between */
  /* the reboot, then register values set to zero, to overcome this */
  /* return TRUE always */
  return TRUE;
}

static gboolean
aum_boot_state_nvmem_set_update_available (AumBootState * boot_state,
                                          gboolean update_available)
{
  gboolean ret = FALSE;

  AumBootStateNvmem *self = AUM_BOOT_STATE_NVMEM (boot_state);
  self->update_available = update_available;
  self->count = 0;

  ret = aum_boot_state_nvmem_update_on_disk (self);

  return ret;
}

static void
aum_boot_state_nvmem_init (AumBootStateNvmem * self)
{
  g_autoptr (GError) error = NULL;
  guchar read_buf[SCRATCH_REG_LEN];
  gint ret;

  memset(read_buf, 0, SCRATCH_REG_LEN);
  ret = nvmem_read_scratch_reg(read_buf);
  if(ret < 0)
      goto err;

  self->count = read_buf[SCRATCH_REG2_BYTE0];
  self->update_available = read_buf[SCRATCH_REG2_BYTE1];
  self->version = read_buf[SCRATCH_REG2_BYTE2];
  self->magic = read_buf[SCRATCH_REG2_BYTE3];
  self->limit = 4;
  self->active = TRUE;

  g_message ("Loaded boot count from nvmem: count %d", self->count);

  return;

err:
  g_message ("Cannot initialize boot-state from nvmem: %s", error->message);
  self->active = FALSE;
  self->count = 0;
  self->limit = 4;
}

static void
aum_boot_state_nvmem_iface_init (AumBootStateInterface *iface)
{
  iface->is_active = aum_boot_state_nvmem_is_active;
  iface->get_name = aum_boot_state_nvmem_get_name;
  iface->get_boot_count = aum_boot_state_nvmem_get_boot_count;
  iface->get_boot_limit = aum_boot_state_nvmem_get_boot_limit;
  iface->set_boot_limit = aum_boot_state_nvmem_set_boot_limit;
  iface->get_update_available = aum_boot_state_nvmem_get_update_available;
  iface->set_update_available = aum_boot_state_nvmem_set_update_available;
}

static void
aum_boot_state_nvmem_class_init (AumBootStateNvmemClass * cls)
{
}
