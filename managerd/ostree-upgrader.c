
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 * Copyright (C) 2015 Red Hat, Inc.
 * Copyright © 2017,2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: ostreeupgrader
 * @title: AumOstreeUpgrader
 * @short_description: Ostree upgrade handler
 * @include: ostree-upgrader.h
 *
 * The ostree upgrader is responsible for polling the remote ostree repository
 * and upgraded the system whenever a new deployment is available.
 */

#include "ostree-upgrader.h"
#include "managerd-enums.h"

#include <ostree.h>
#include <errno.h>

#define OSTREE_STATIC_DELTA_META_ENTRY_FORMAT "(uayttay)"
#define OSTREE_STATIC_DELTA_FALLBACK_FORMAT "(yaytt)"
#define OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT "(a{sv}tayay" \
	OSTREE_COMMIT_GVARIANT_STRING "aya" \
	OSTREE_STATIC_DELTA_META_ENTRY_FORMAT \
	"a" OSTREE_STATIC_DELTA_FALLBACK_FORMAT ")"

#define OSTREE_STATIC_DELTA_SIGNED_FORMAT "(taya{sv})"
#define OSTREE_STATIC_DELTA_SIGNED_MAGIC  0x4F535453474E4454 /* OSTSGNDT */

#define BLACKLIST "/var/aum_blacklist.conf"

#define OSTREE_SIGN_NAME_ED25519 "ed25519"
#define REMOTE_NAME "origin"

enum
{
  OSTREE_UPGRADE_FAILED = -1,
  OSTREE_UPGRADE_NO_UPDATE,
  OSTREE_UPGRADE_AVAILABLE,
  OSTREE_UPGRADE_READY,
};

enum
{
  PROP_UPGRADE_STATE = 1,
  NUM_PROPERTIES
};

enum
{
  DELTA_METADATA = 0,
  DELTA_TIMESTAMP,
  DELTA_CHECKSUM_FROM,
  DELTA_CHECKSUM_TO,
  DELTA_COMMIT,
  DELTA_ARRAY_FROM_TO,
  DELTA_HEADERS,
  DELTA_FALLBACK
};

enum
{
  COMMIT_METADATA = 0,
  COMMIT_PARENT_CHECKSUM,
  COMMIT_RELATED,
  COMMIT_SUBJECT,
  COMMIT_BODY,
  COMMIT_TIMESTAMP,
  COMMIT_ROOT_TREE,
  COMMIT_ROOT_TREE_METADATA
};

typedef enum
{
  UNSIGNED_DELTA = 0,
  SIGNED_DELTA
} offline_delta_type;

struct _AumOstreeUpgrader
{
  GObject parent;
  gboolean active;
  gboolean updating;
  gboolean dry_run;
  guint timeout_id;
  guint poll_seconds;
  OstreeSysroot *sysroot;
  gchar *static_delta;
  GTask *static_delta_task;

  AumOstreeUpgradeState upgrade_state;
};

static void
aum_ostree_upgrader_init (AumOstreeUpgrader * self)
{
  g_autoptr (GError) error = NULL;

  self->sysroot = ostree_sysroot_new_default ();

  if (!ostree_sysroot_load (self->sysroot, NULL, &error))
    {
      g_warning ("Ostree failed to load sysroot: %s", error->message);
      g_warning ("No Ostree updates will be done");
      g_object_unref (self->sysroot);
      self->sysroot = NULL;
    }
}

static void
aum_ostree_upgrader_dispose (GObject * object)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  if (self->timeout_id > 0)
    {
      g_source_remove (self->timeout_id);
    }

  if (self->sysroot)
    {
      g_object_unref (self->sysroot);
    }

  g_free (self->static_delta);
}

static void
aum_ostree_upgrader_get_property (GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  switch (prop_id)
    {
      case PROP_UPGRADE_STATE:
        g_value_set_enum (value, self->upgrade_state);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}


static GParamSpec *props[NUM_PROPERTIES] = { NULL, };

static void
aum_ostree_upgrader_class_init (AumOstreeUpgraderClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_UPGRADE_STATE] =
    g_param_spec_enum ("upgrade-state",
                       "Upgrade State",
                       "The current upgrade state",
                       AUM_TYPE_OSTREE_UPGRADE_STATE,
                       AUM_OSTREE_UPGRADE_STATE_UNKNOWN,
                       G_PARAM_READABLE);

  object_class->get_property = aum_ostree_upgrader_get_property;
  object_class->dispose = aum_ostree_upgrader_dispose;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

static void _ostree_upgrader_start_update (AumOstreeUpgrader * self);

static void
_ostree_upgrader_set_upgrade_state (AumOstreeUpgrader * self,
                                    AumOstreeUpgradeState state)
{
  if (state != self->upgrade_state)
    {
      self->upgrade_state = state;
      g_object_notify_by_pspec (G_OBJECT (self), props[PROP_UPGRADE_STATE]);
    }
}

gboolean
_ostree_upgrader_poll_timeout_cb (gpointer user_data)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (user_data);

  self->timeout_id = 0;
  _ostree_upgrader_start_update (self);

  return G_SOURCE_REMOVE;
}

static void
_ostree_upgrader_update_cb (GObject * object,
                            GAsyncResult * res, gpointer user_data)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  g_autoptr (GError) error = NULL;
  gssize ret;

  ret = g_task_propagate_int (G_TASK (res), &error);

  switch (ret)
    {
    case OSTREE_UPGRADE_FAILED:
      g_message ("Ostree upgrade failed: %s", error->message);
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_UNKNOWN);

      break;
    case OSTREE_UPGRADE_NO_UPDATE:
      g_message ("Ostree already up to date");
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_UPTODATE);
      break;
    case OSTREE_UPGRADE_AVAILABLE:
      g_message ("Ostree upgrade is available");
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_AVAILABLE);
      break;
    case OSTREE_UPGRADE_READY:
      g_message ("Ostree upgrade ready, system should be rebooted");
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_PENDING);
      break;
    }

  self->updating = FALSE;
  g_clear_pointer (&self->static_delta, g_free);
  g_clear_object (&self->static_delta_task);

  if (self->active && self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
    {
      self->timeout_id = g_timeout_add_seconds (self->poll_seconds,
                                                _ostree_upgrader_poll_timeout_cb,
                                                self);
    }
  g_object_unref (self);
}

static void
_ostree_upgrader_update_thread (GTask * task,
                                gpointer source,
                                gpointer task_data,
                                GCancellable * cancellable)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (source);
  GError *error = NULL;
  g_autoptr (OstreeSysrootUpgrader) upgrader = NULL;
  g_autoptr (OstreeAsyncProgress) progress = NULL;
  GKeyFile *origin = NULL;
  GMainContext *task_context = NULL;
  gboolean ret;
  gboolean changed = FALSE;
  gboolean deployed = FALSE;

  g_autoptr (OstreeRepo) repo = NULL;
  g_autofree char *origin_refspec = NULL;
  g_autofree char *rev = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  gboolean blacklisted = FALSE;
  int i;
  gssize ret_state = OSTREE_UPGRADE_NO_UPDATE;

  task_context = g_main_context_new ();
  g_main_context_push_thread_default (task_context);

  upgrader = ostree_sysroot_upgrader_new (self->sysroot, NULL, &error);
  if (upgrader == NULL)
    {
      g_task_return_error (task, error);
      goto err;
    }

  /* Need to cleanup override-commit field from origin file.
   * In other case it would be used as ref to download */
  origin = ostree_sysroot_upgrader_dup_origin(upgrader);
  if (origin != NULL)
    {
      ostree_deployment_origin_remove_transient_state (origin);
      if (!ostree_sysroot_upgrader_set_origin (upgrader, origin, NULL, &error))
        {
          g_warning ("Unable to clear state of origin for upgrade");
          g_task_return_error (task, error);
          goto err;
        }
    }

  progress =
    ostree_async_progress_new_and_connect (ostree_repo_pull_default_console_progress_changed,
                                           self);

  /*Download commit's metadata first and check in blacklist */
  if (! ostree_sysroot_upgrader_pull (upgrader,
                                      OSTREE_REPO_PULL_FLAGS_COMMIT_ONLY,
                                      0, progress, &changed,
                                      NULL, &error))
    {
      ostree_sysroot_cleanup (self->sysroot, NULL, NULL);
      g_task_return_error (task, error);
      goto err;
    }

  if (progress)
    ostree_async_progress_finish (progress);

  if (!changed)
    {
      goto out;
    }

  /* Show the availability in the upgrade state */
  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_AVAILABLE);
  g_message ("Network upgrade is available");

  /* Ostree considers things to have changed if the last version is
   * different from the booted deployment. Figure out if that last revision
   * was already deployed and if so consider no changes */
  origin_refspec = g_key_file_get_string (origin,
                                          "origin", "refspec",
                                          NULL);
  ret = ostree_sysroot_get_repo (self->sysroot, &repo, NULL, &error);
  if (!ret)
    {
      g_task_return_error (task, error);
      goto err;
    }

  ret =
    ostree_repo_resolve_rev (repo, origin_refspec, FALSE, &rev, &error);
  if (!ret)
    {
      g_task_return_error (task, error);
      goto err;
    }

  /* Check if commit is not blacklisted */
  blacklisted = aum_ostree_upgrader_check_blacklisted(BLACKLIST, rev);
  if (blacklisted)
    {
      ostree_async_progress_finish (progress);
      g_warning ("Revision '%s' is marked as blacklisted; skipping", rev);
      changed = FALSE;
      goto out;
    }

  deployments = ostree_sysroot_get_deployments (self->sysroot);
  for (i = 0; i < deployments->len; i++)
    {
      OstreeDeployment *d = g_ptr_array_index (deployments, i);
      const gchar *csum = ostree_deployment_get_csum (d);

      if (!g_strcmp0 (rev, csum))
        {
          g_message
            ("Latest revision already deployed; just pending reboot");
          deployed = TRUE;
          changed = FALSE;
        }
    }

  /* Check if we are in dry-run mode,
   * do not need to proceed with the upgrade */
  if (self->dry_run)
    {
      g_debug ("dry-run mode: skip the upgrade");
      changed = FALSE;
      goto out;
    }

  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_DOWNLOADING);

  progress =
    ostree_async_progress_new_and_connect (ostree_repo_pull_default_console_progress_changed,
                                           self);

  /* Download the commit itself */
  ret = ostree_sysroot_upgrader_pull (upgrader, 0, 0,
                                      progress, &changed,
                                      NULL, &error);
  if (!ret)
    {
      ostree_sysroot_cleanup (self->sysroot, NULL, NULL);
      g_task_return_error (task, error);
      goto err;
    }

  if (progress)
    ostree_async_progress_finish (progress);

  if (!deployed)
    {
      g_message ("New upgrade downloaded! Deploying..");

      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_DEPLOYING);

      ret = ostree_sysroot_upgrader_deploy (upgrader, NULL, &error);
      if (!ret)
        {
          g_task_return_error (task, error);
          goto err;
        }
    }

out:
  if (changed)
    ret_state = OSTREE_UPGRADE_READY;
  else if (self->dry_run)
    ret_state = OSTREE_UPGRADE_AVAILABLE;

  g_task_return_int (task, ret_state);

err:
  g_main_context_pop_thread_default (task_context);
  g_main_context_unref (task_context);
}

static void
_ostree_upgrader_start_update (AumOstreeUpgrader * self)
{
  GTask *task = NULL;

  if (self->updating || self->sysroot == NULL)
    return;

  g_message ("Ostree upgrade poll starting");
  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_CHECKING);


  self->updating = TRUE;
  task = g_task_new (self, NULL, _ostree_upgrader_update_cb, NULL);
  g_object_ref (self);
  g_task_run_in_thread (task, _ostree_upgrader_update_thread);
}

void
aum_ostree_upgrader_set_active (AumOstreeUpgrader * self,
                                gboolean active,
                                guint poll_seconds)
{
  self->poll_seconds = poll_seconds;

  if (self->active == active)
    return;

  self->active = active;
  if (active)
    {
      if (self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
        {
          _ostree_upgrader_start_update (self);
        }
    }
  else
    {
      /* On deactivation just simply drop the timer if running, an
       * ongoing update will error out by itself if the network fails
       * for some reason */
      if (self->timeout_id > 0)
        {
          g_source_remove (self->timeout_id);
        }
    }
}

void
aum_ostree_upgrader_set_ready (AumOstreeUpgrader *self)
{
  GSource *source;

  if (self->timeout_id <= 0)
    return;

  source = g_main_context_find_source_by_id (NULL, self->timeout_id);
  if (source == NULL) {
    /* Already destroyed */
    g_warning ("Source ID %u was not found", self->timeout_id);
    return;
  }

  g_source_set_ready_time (source, 0);
}

void
aum_ostree_upgrader_set_dry_run (AumOstreeUpgrader *self, gboolean mode)
{
  self->dry_run = mode;
}

static gboolean
_aum_load_keys(OstreeRepo *repo,
               const gchar *remote_name,
               OstreeSign *sign,
               GError **error)
{
  g_autofree gchar *pk_ascii = NULL;
  g_autofree gchar *pk_file = NULL;
  gboolean loaded_from_file = FALSE;

  /* Load keys for remote from file if option exists */
  ostree_repo_get_remote_option (repo,
                                 remote_name,
                                 "verification-ed25519-file",
                                 NULL,
                                 &pk_file,
                                 NULL);
  if (pk_file != NULL)
    {
      g_autoptr (GVariantBuilder) builder = NULL;
      g_autoptr (GVariant) options = NULL;

      builder = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));
      g_variant_builder_add (builder, "{sv}", "filename", g_variant_new_string (pk_file));
      options = g_variant_builder_end (builder);

      /* If the filename is explicitly provided in config but it is not in working state -- do not proceed further */
      if (!ostree_sign_load_pk (sign, options, error))
        goto out;

      loaded_from_file = TRUE;
    }

  /* Load key for remote from config if option exists */
  ostree_repo_get_remote_option (repo,
                                 remote_name,
                                 "verification-ed25519-key",
                                 NULL,
                                 &pk_ascii,
                                 NULL);
  if (pk_ascii != NULL)
    {
      g_autoptr (GVariant) pk = g_variant_new_string(pk_ascii);
      gboolean loaded_inlined = FALSE;

      /* Add inlined public key */
      if (loaded_from_file)
        loaded_inlined = ostree_sign_add_pk (sign, pk, error);
      else
        loaded_inlined = ostree_sign_set_pk (sign, pk, error);

      /* Stop upgrade if inlined key is wrong */
      if (!loaded_inlined)
        goto out;
    }

  return TRUE;

out:
  if (*error == NULL)
      g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                           "Can't load keys");

  return FALSE;
}

/**
 * aum_sign_engine_init:
 * @repo: OstreeRepo
 * @cancellable: cancellable
 * @error: error
 *
 * Initialize the signing engine "ed25519" for remote "origin"
 *
 * Returns: (transfer full): New signing engine, or %NULL if the engine is not known
 */
OstreeSign *
aum_sign_engine_init (OstreeRepo *repo,
                      GCancellable *cancellable,
                      GError **error)
{
  g_autoptr (OstreeSign) sign = NULL;

  if ((sign = ostree_sign_get_by_name (OSTREE_SIGN_NAME_ED25519, error)) == NULL)
    goto out;

  if (!_aum_load_keys(repo, REMOTE_NAME, sign, error))
    goto out;

  return g_steal_pointer(&sign);

out:
  return NULL;
}

/**
 * @brief Load superblock from static delta
 *
 * @param[IN] filename -- path to upgrade file
 * @param[OUT] superblock -- superblock of upgrade file
 * @param[IN,OUT] delta_type -- use signed superblock
 * @param error -- contain error if any
 *
 * @return true if superblock is available.
 */
gboolean
aum_load_target_superblock (const gchar *filename,
                            GVariant **superblock,
                            offline_delta_type *delta_type,
                            GError **error)
{
  g_autoptr (GMappedFile) map = NULL;
  g_autoptr (GBytes) bytes = NULL;
  g_autoptr (GVariant) delta = NULL;
  g_autoptr (GBytes) meta_bytes = NULL;

  g_return_val_if_fail (*superblock == NULL, FALSE);

  map = g_mapped_file_new (filename, FALSE, error);
  if (!map)
    goto out;

  bytes = g_mapped_file_get_bytes (map);
  if (!bytes)
    goto out;

  /* Try signed delta format first */
  delta = g_variant_new_from_bytes (G_VARIANT_TYPE(OSTREE_STATIC_DELTA_SIGNED_FORMAT), bytes, FALSE);
  if (delta)
    {
      g_autoptr (GVariant) delta_sign_magic = g_variant_get_child_value (delta, 0);
      if (delta_sign_magic != NULL)
        {
          if (GUINT64_FROM_BE (g_variant_get_uint64 (delta_sign_magic)) == OSTREE_STATIC_DELTA_SIGNED_MAGIC)
            {
              g_autoptr (GVariant) signed_superblock = g_variant_get_child_value (delta, 1);
              meta_bytes = g_variant_get_data_as_bytes (signed_superblock);
              g_debug("%s : %s", __FUNCTION__, "found signed static-delta");
              *delta_type = SIGNED_DELTA;
            }
          else
            {
              /* Explicitly warn user about unsigned delta format */
              g_warning("Proceed with unsigned upgrade bundle");
            }
        }
    }

  if (!meta_bytes)
    {
      /* check if signed bundle is mandatory */
      if (*delta_type == SIGNED_DELTA)
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "Signed static-delta format is required");
          goto out;
        }
      meta_bytes = g_bytes_ref(bytes);
    }

  /*
   * If `core.sign-verify-deltas` option is set in repository configuration,
   * the unsigned delta would be rejected by libostree.
   */
  *superblock = g_variant_new_from_bytes (G_VARIANT_TYPE(OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT),
                                          meta_bytes, FALSE);

  if (!superblock)
    goto out;

  return TRUE;

out:
  if (error && *error)
    g_prefix_error (error, "Invalid file: ");
  else
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, "Invalid file");

  return FALSE;
}

static gboolean
aum_get_metadata_collection_refs (GVariant *metadata,
                                  char **out_collection_id,
                                  char ***out_refs,
                                  GError **error)
{
  const char *collection_id = NULL;
  char **refs = NULL;

  g_return_val_if_fail(metadata, FALSE);
  g_return_val_if_fail(*out_collection_id == NULL, FALSE);
  g_return_val_if_fail(*out_refs == NULL, FALSE);

  /* In case if collection_id is not found, the latter functions will work
   * as the old pre-collection versions */
  if (g_variant_lookup (metadata,
                         OSTREE_COMMIT_META_KEY_COLLECTION_BINDING,
                         "&s",
                         &collection_id))
    {
      if (!ostree_validate_collection_id (collection_id, error))
        {
          g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                               "wrong Collection ID name");
          return FALSE;
        }
      *out_collection_id = g_strdup(collection_id);
    }

  // Refs
  /* It is possible that commit doesn't have refs */
  if (g_variant_lookup (metadata,
                        OSTREE_COMMIT_META_KEY_REF_BINDING,
                        "^a&s",
                        &refs))
    {
      *out_refs = g_strdupv(refs);

      for (char **ref = refs; *ref != NULL; ++ref)
	g_debug ("metadata ref: %s:%s", collection_id, *ref);
    }
  else
    {
      g_debug ("no refs in commit metadata");
    }

  return TRUE;
}


/**
 * @brief Collect needed info from static delta
 *
 * It is possible that commit doesn't contain any metadata
 * and/or required metadata keys. This function only parse
 * the commit and collect needed information. The analysis
 * should be done in other places.
 *
 * @param[IN] superblock -- superblock of upgrade file
 * @param[OUT] checksum -- checksum of target commit
 * @param[OUT] collection_id -- Collection ID used in commit metadata
 * @param[OUT] refs -- refs found in the commit metadata
 * @param[OUT] timestamp -- timestamp of commit
 * @param error -- contain error if any
 *
 * @return true if inlined commit is correct and could be parsed.
 */
gboolean
aum_get_target_metadata (GVariant *superblock,
                         char **checksum,
                         char **out_collection_id,
                         char ***out_refs,
                         guint64 *timestamp,
                         GError **error)
{
  g_autoptr (GVariant) csum_v = NULL;
  g_autoptr (GVariant) commit = NULL;
  g_autoptr (GVariant) metadata = NULL;

  g_return_val_if_fail(*checksum == NULL, FALSE);
  g_return_val_if_fail(*out_collection_id == NULL, FALSE);
  g_return_val_if_fail(*out_refs == NULL, FALSE);

  csum_v = g_variant_get_child_value (superblock, DELTA_CHECKSUM_TO);
  if (!csum_v)
    goto out;

  if (!ostree_validate_structureof_csum_v (csum_v, error))
    goto out;

  *checksum = ostree_checksum_from_bytes_v (csum_v);

  // Parse the inlined commit.
  commit = g_variant_get_child_value (superblock, DELTA_COMMIT);
  if (!ostree_validate_structureof_commit(commit, error))
      goto out;

  // Read needed values from commit's metadata
  metadata = g_variant_get_child_value (commit, COMMIT_METADATA);
  g_message ("Metadata read from commit '%s': %s",
           *checksum,
           g_variant_print(metadata, TRUE));

  *timestamp = ostree_commit_get_timestamp(commit);

  return aum_get_metadata_collection_refs (metadata, out_collection_id, out_refs, error);

out:
  if (error && *error)
    g_prefix_error (error, "Invalid file: ");
  else
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, "Invalid file");

  return FALSE;
}

/**
 * @brief Check if there is any remote with enabled Collection ID
 *
 * Apertis is using only the single remote 'origin', so it is enough to know
 * that this remote is configured to use Collection ID information.
 * Do not care about the Collection ID itself since it would be used automatically
 * during validation of Collection ID provided in update commit.
 *
 * @param repo -- repository
 *
 * @return TRUE if any remote is configured to use Collection ID
 */
gboolean
aum_remote_collection_id_is_set(OstreeRepo *repo)
{
  g_auto(GStrv) remotes = NULL;
  guint n_remotes = 0;
  g_autofree gchar *remote_collection_id = NULL;

  remotes = ostree_repo_remote_list(repo, &n_remotes);

  for (guint i = 0; i < n_remotes; i++)
    {
      // Check if that remote is configured to have collection_id
      // atm we do not care about the name
      if (ostree_repo_get_remote_option (repo,
                                         remotes[i],
                                         "collection-id",
                                         NULL,
                                         &remote_collection_id,
                                         NULL) &&
          remote_collection_id != NULL)
        {
          if (ostree_validate_collection_id (remote_collection_id, NULL))
            g_debug("Remote %s: collection-id=%s", remotes[i], remote_collection_id);

          return TRUE;
        }
    }

  return FALSE;
}

/**
 * @brief Check if Collection refs are applicable for repo
 *
 * Check if Collection ID and at least a single ref name
 * is suitable for local repository.
 *
 * @param repo
 * @param collection_id
 * @param refs
 * @param[OUT] ref_name -- name of applicable ref
 * @param error
 *
 * @return TRUE if Collection ID and any ref are valid,
 *         ref_name is initialised with suitable name.
 */
gboolean
aum_validate_collection_refs (OstreeRepo *repo,
                              char *collection_id,
                              char **refs,
                              char **ref_name,
                              GError **error)
{
  gboolean found = FALSE;

  g_return_val_if_fail(repo != NULL, FALSE);
  g_return_val_if_fail(*ref_name == NULL, FALSE);

  if (collection_id == NULL || refs == NULL)
    goto out;

  for (char **ref = refs; *ref != NULL; ++ref)
    {
      const OstreeCollectionRef collection_ref = { (char *) collection_id, (char *) *ref };

      g_debug("Checking ref: %s", *ref);
      if (ostree_repo_resolve_collection_ref (repo, &collection_ref,
                                              FALSE,
                                              OSTREE_REPO_RESOLVE_REV_EXT_NONE,
                                              NULL,
                                              NULL,
                                              NULL))
        {
          // Stop as soon as we found ref set in repo
          found = TRUE;
          *ref_name = g_strdup(*ref);
          break;
        }
    }

out:
  if (!found)
    {
      g_autoptr(GHashTable) local_refs = NULL;

      if (ostree_repo_list_collection_refs (repo, collection_id, &local_refs,
                                            OSTREE_REPO_LIST_REFS_EXT_NONE,
                                            NULL, NULL))
        {
          GHashTableIter hashiter;
          gpointer hashkey, hashvalue;
          g_autofree gchar *ref_list = g_strjoinv(", ", refs);

          g_hash_table_iter_init (&hashiter, local_refs);
          while (g_hash_table_iter_next (&hashiter, &hashkey, &hashvalue))
            {
              const OstreeCollectionRef *ref = hashkey;
              g_message ("expecting '%s' but bundle is for '%s'", ref->ref_name, ref_list);
            }

        }

      g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                   "Couldn't find applicable ref name for Collection ID (%s)",
                   collection_id);

    }

  return found;
}

gboolean
aum_validate_timestamp(OstreeRepo *repo,
                       OstreeDeployment *booted_deployment,
                       guint64 commit_ts,
                       GError **error)
{
  g_autoptr (GVariant) booted_commit = NULL;
  if (!ostree_repo_load_commit (repo,
                                ostree_deployment_get_csum(booted_deployment),
                                &booted_commit,
                                NULL,
                                error))
    {
      goto out;
    }

  const guint64 booted_ts = ostree_commit_get_timestamp (booted_commit);
  if ( commit_ts <= booted_ts)
    {
      goto out;
    }

  return TRUE;

out:
  return FALSE;
}

void
_ostree_upgrader_static_delta_thread (GTask * task,
                                      gpointer source,
                                      gpointer task_data,
                                      GCancellable *cancellable)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (source);
  GMainContext *task_context = NULL;
  GError* error = NULL;
  g_autoptr (GVariant) superblock = NULL;
  g_autoptr (GError) cleanup_err = NULL;
  g_autoptr (GFile) file = NULL;
  g_autoptr (OstreeRepo) repo = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  g_autoptr (OstreeDeployment) new_deployment = NULL;
  g_autofree gchar *checksum = NULL;
  g_autofree char *origin_refspec = NULL;
  OstreeDeployment *merge_deployment = NULL;
  OstreeDeployment *booted_deployment = NULL;
  const gchar *osname = NULL;
  GKeyFile *origin = NULL;
  gboolean changed = TRUE;
  gboolean blacklisted = FALSE;
  g_autofree gchar *collection_id = NULL;
  g_autofree gchar *ref_name = NULL;
  g_auto (GStrv) refs = NULL;
  guint64 commit_ts = 0L;
  offline_delta_type delta_type = UNSIGNED_DELTA;
  GKeyFile *config = NULL;
  g_autoptr (OstreeSign) sign = NULL;
  gboolean check_sign_commit = FALSE;

  task_context = g_main_context_new ();
  g_main_context_push_thread_default (task_context);

  booted_deployment = ostree_sysroot_get_booted_deployment (self->sysroot);

  if (!ostree_sysroot_get_repo (self->sysroot, &repo, NULL, &error))
    goto out;

  /* Initialize signature engine first */
  sign = aum_sign_engine_init (repo, cancellable, &error);
  if (!sign)
    goto out;

  config = ostree_repo_get_config (repo);
  /* Check if repo configuration is set for accepting only signed deltas */
  if (g_key_file_get_boolean (config,
                              "core",
                              "sign-verify-deltas",
                              NULL))
    {
      delta_type = SIGNED_DELTA;
    }
  else
    {
      g_warning("Global configuration allows to use unsigned upgrade deltas");
    }

  if (!aum_load_target_superblock(self->static_delta, &superblock, &delta_type, &error))
    {
      changed = FALSE;
      goto out;
    }

  /* Collect info from upgrade file */
  if (!aum_get_target_metadata (superblock,
                                &checksum,
                                &collection_id,
                                &refs,
                                &commit_ts,
                                &error))
    {
      changed = FALSE;
      goto out;
    }

  /* Chance for the systems without Collection ID used.
   * Allow to proceed with update for them even if there is
   * no collections in update file.
   */
  if (aum_remote_collection_id_is_set(repo))
    {
      /* Check if static delta is applicable for current system */
      if (!aum_validate_collection_refs (repo,
                                         collection_id,
                                         refs,
                                         &ref_name,
                                         &error))
        {
          changed = FALSE;
          goto out;
        }
    }

  g_debug ("Collection ref: %s:%s", collection_id, ref_name);

  /* Check if timestamp of commit is newer than current deployment */
  if (!aum_validate_timestamp (repo,
                               booted_deployment,
                               commit_ts,
                               &error))
    {
      changed = FALSE;
      goto out;
    }

  /* Check if commit is not blacklisted */
  blacklisted = aum_ostree_upgrader_check_blacklisted(BLACKLIST, checksum);
  if (blacklisted)
    {
      g_warning ("Revision '%s' is marked as blacklisted; skipping", checksum);
      changed = FALSE;
      goto out;
    }

  /* Check if we already do not use the commit ID */
  deployments = ostree_sysroot_get_deployments (self->sysroot);
  for (int i = 0; i < deployments->len; i++)
    {
      OstreeDeployment *d = g_ptr_array_index (deployments, i);
      const gchar *csum = ostree_deployment_get_csum (d);

      if (!g_strcmp0 (checksum, csum))
        {
          g_message("Static delta already applied; skipping");
          changed = FALSE;
          goto out;
        }
    }


  /* Prepare a commit */
  if (!ostree_repo_prepare_transaction (repo, NULL, cancellable, &error))
    goto out;

  /* Create a local reference for the checksum */
  if (ref_name == NULL)
    ref_name = g_strdup("master");
  ostree_repo_transaction_set_ref (repo,
                                   NULL,
                                   ref_name,
                                   checksum);
  /* TODO: switch later, as soon as ostree CLI will be ready */
  /* Create a reference based on collection id ref pair detected from static delta. */
  // ostree_repo_transaction_set_collection_ref (repo, &self->collection_ref, self->checksum);

  file = g_file_new_for_path (self->static_delta);

  if (!ostree_repo_static_delta_execute_offline_with_signature (repo, file,
                                                                (delta_type == SIGNED_DELTA) ? sign : NULL,
                                                                FALSE, cancellable, &error))
    {
      ostree_repo_abort_transaction (repo, cancellable, NULL);
      goto out;
    }

  /* Get from configuration if we need signature check */
  ostree_repo_get_remote_boolean_option (repo,
                                         REMOTE_NAME,
                                         "sign-verify",
                                         FALSE,
                                         &check_sign_commit,
                                         NULL);
  if (check_sign_commit)
    {
      if (!ostree_sign_commit_verify (sign,
                                      repo,
                                      checksum,
                                      NULL,
                                      cancellable,
                                      &error))
        {
          g_message("Commit in static delta have no valid signature; aborted");
          ostree_repo_abort_transaction (repo, cancellable, NULL);
          changed = FALSE;
          goto out;
        }
    }
   else
     g_warning ("Commit signature verification is switched off");

  g_message("Static delta is applicable for the system");
  if (self->dry_run)
    {
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_AVAILABLE);
      g_debug ("dry-run mode: skip the upgrade");
      changed = FALSE;
      ostree_repo_abort_transaction (repo, cancellable, NULL);
      goto out;
    }

  _ostree_upgrader_set_upgrade_state (self,
				       AUM_OSTREE_UPGRADE_STATE_DEPLOYING);

  /* Commit the transaction */
  if (!ostree_repo_commit_transaction (repo, NULL, cancellable, &error))
    goto out;

  osname = ostree_deployment_get_osname (booted_deployment);
  merge_deployment = ostree_sysroot_get_merge_deployment (self->sysroot, osname);
  origin = ostree_deployment_get_origin (merge_deployment);

  /* deploy the commit
   * ostree_sysroot_deploy_tree garbage collects all deployments */

  if (!ostree_sysroot_deploy_tree (self->sysroot, osname,
                                   checksum,
                                   origin,
                                   merge_deployment,
                                   NULL,
                                   &new_deployment,
                                   cancellable, &error))
    goto out;

  /* Update the boot files */
  if (!ostree_sysroot_simple_write_deployment (self->sysroot, osname,
                                               new_deployment,
                                               merge_deployment,
                                               0,
                                               cancellable, &error))
    goto out;

out:
  /* Remove staged temporary directory */
  /* NB: The cleanup call below do the full repository prune, not only temporary directory */
  /* NB2: we do not want mark the upgrade process as failed in case of any error acquired during cleanup. */
  if (!ostree_sysroot_cleanup (self->sysroot, cancellable, &cleanup_err))
      if (cleanup_err)
        {
          g_warning ("Cleanup problem: %s\n", cleanup_err->message);
        }

  if (error)
    g_task_return_error (task, error);
  else
    g_task_return_int (task,
                     changed ? OSTREE_UPGRADE_READY
                             : OSTREE_UPGRADE_NO_UPDATE);

  g_main_context_pop_thread_default (task_context);
  g_main_context_unref (task_context);
}

void
aum_ostree_upgrade_set_state(AumOstreeUpgrader *self,
			     AumOstreeUpgradeState state)
{
  _ostree_upgrader_set_upgrade_state (self, state);
}

gboolean
aum_ostree_upgrade_apply_static_delta(AumOstreeUpgrader *self,
                                      const gchar *filename)
{
  GCancellable *cancellable;

  if (self->updating || self->sysroot == NULL)
    return FALSE;

  g_message ("Ostree static delta starting");
  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_CHECKING);

  self->updating = TRUE;
  self->static_delta = g_strdup (filename);

  cancellable = g_cancellable_new ();
  self->static_delta_task = g_task_new (self, cancellable, _ostree_upgrader_update_cb, NULL);
  g_object_ref (self);
  g_object_unref (cancellable);
  g_task_run_in_thread (self->static_delta_task, _ostree_upgrader_static_delta_thread);

  return TRUE;
}

gboolean
aum_ostree_upgrader_static_delta_is_running (AumOstreeUpgrader *self)
{
  if (self->static_delta_task && g_task_is_valid(self->static_delta_task, self))
    return !g_task_get_completed (self->static_delta_task);

  return FALSE;
}

void
aum_ostree_upgrader_static_delta_cancel (AumOstreeUpgrader *self)
{
  GCancellable *cancellable;

  cancellable = g_task_get_cancellable (self->static_delta_task);
  g_assert_true (G_IS_CANCELLABLE (cancellable));

  if (g_cancellable_is_cancelled (cancellable))
    return;

  g_message ("Cancelling static delta upgrade");

  g_cancellable_cancel (cancellable);
}

void
aum_ostree_upgrade_apply_pending(AumOstreeUpgrader *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusProxy) systemd_proxy = NULL;

  // Reboot only if update is applied
  if (self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
    return;

  g_debug("Connecting to systemd ...\n");
  systemd_proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                                 G_DBUS_PROXY_FLAGS_NONE,
                                                 NULL,
                                                 "org.freedesktop.systemd1",
                                                 "/org/freedesktop/systemd1",
                                                 "org.freedesktop.systemd1.Manager",
                                                 NULL,
                                                 &error);
  if (!systemd_proxy)
    goto out;

  g_message ("Rebooting to apply pending update");
  g_dbus_proxy_call_sync (systemd_proxy,
                          "Reboot",
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          30000,
                          NULL,
                          &error);

out:
  if (error)
    {
      g_warning("Failed to invoke reboot: %s\n", error->message);
    }
}

gboolean
aum_ostree_upgrader_undeploy(AumOstreeUpgrader *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  g_autoptr (OstreeDeployment) pending_deployment = NULL;

  g_message ("Undeploy requested");

  deployments = ostree_sysroot_get_deployments (self->sysroot);
  ostree_sysroot_query_deployments_for (self->sysroot, NULL, &pending_deployment, NULL);

  if ( pending_deployment == NULL )
    {
      /* FIXME: how to handle situation with rollback 
       * but without pending updates? */
      g_message ("No pending updates");
      goto out;
    }

  /* Add the ID into black list */
  aum_ostree_upgrader_set_blacklisted (BLACKLIST,
                                       ostree_deployment_get_csum(pending_deployment));

  if (!g_ptr_array_remove (deployments, pending_deployment))
    {
      /* FIXME: possible races? */
      g_message ("Failed to remove deployment from the list");
      goto err;
    }

  if (!ostree_sysroot_write_deployments (self->sysroot,
                                         deployments, NULL, &error))
      goto err;

  if (!ostree_sysroot_cleanup (self->sysroot, NULL, &error))
    goto err;

out:
  return TRUE;

err:
  g_message ("Failed to undeploy: %s", error ? error->message : "unknown error");
  return FALSE;
}

/**
 * @brief Check commit ID in blacklist configuration
 *
 * If we can't load the file or get ID from it by any reason
 * we are not able to check if the commit is in list.
 * So permit to use the commit in that case.
 *
 * @param filename
 * @param checksum
 *
 * @return TRUE if blacklisted
 */
gboolean
aum_ostree_upgrader_check_blacklisted(const gchar *filename,
                                      const gchar *checksum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) blfile = NULL;
  gboolean blacklisted = FALSE;

  blfile = g_key_file_new ();

  if (!g_key_file_load_from_file (blfile, filename, G_KEY_FILE_NONE, &error))
      goto out;

  /* Return FALSE if not found, could not be parsed or set to FALSE externally */
  blacklisted = g_key_file_get_boolean (blfile, "blacklist", checksum, NULL);

out:
  if (error)
    g_warning ("Cannot check the ID in black list: %s", error->message);
  return blacklisted;
}

/**
 * @brief Mark ID as blacklisted
 *
 * @param filename
 * @param checksum
 */
void
aum_ostree_upgrader_set_blacklisted(const gchar *filename,
                                      const gchar *checksum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) blfile = NULL;

  blfile = g_key_file_new ();

  if (g_file_test(filename, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
    {
      if (!g_key_file_load_from_file (blfile, filename, G_KEY_FILE_NONE, &error))
        goto out;
    }

  g_key_file_set_boolean (blfile, "blacklist", checksum, TRUE);

  g_key_file_save_to_file (blfile, filename, &error);

out:
  if (error)
    g_warning ("Cannot add the ID into black list: %s", error->message);
}

/* The refs list of OSTree branches is alphabetically ordered, which is not
 * what we expect for Apertis releases as e.g. 'v2022' comes after 'v2022dev3'
 * and 'v2022pre'
 *
 * Branches are defined by <os>/<release>/<arch>-<board>/<type>
 */
static char*
aum_get_target_ref(const GStrv refs)
{
  const char *target_ref = NULL;
  g_auto (GStrv) target_ref_split = NULL;
  char* const* current;

  if (!refs)
    {
      g_warning ("Invalid ref-bindings");
      return NULL;
    }

  for (current = refs; *current; current++)
    {
      GStrv current_split = g_strsplit (*current, "/", -1);

      if (g_strv_length (current_split) < 4)
        {
          g_strfreev (current_split);
          g_warning ("Invalid ref-binding: %s", *current);
          return NULL;
        }

      /* Use current as target_ref unless target_ref release version is a
       * prefix of the current one
       */

      if (target_ref_split &&
          g_str_has_prefix (current_split[1], target_ref_split[1]))
        {
          g_strfreev (current_split);
          continue;
        }

      target_ref = *current;
      g_strfreev (target_ref_split);
      target_ref_split = current_split;
    }

  return g_strdup (target_ref);
}

/* aum_ostree_upgrader_set_refs:
 * @self: an AumOstreeUpgrader object
 *
 * Update the default branch for remote from the booted commit
 *
 * Check if the booted commit contains the new branch
 * in metadata and update the default branch with a freshest version.
 * This allows to switch the system to new branch (release) smoothly
 * with a static bundle.
 *
 * To switch to the new branch the static bundle should to include the
 * commit containing both branches in metadata -- the current one allowing
 * to be installed and the newer one.
 * On the first successful boot this function should be called to set up
 * default branch to the new version.
 *
 * For Apertis naming schema it is not enough to rely on default alphabetic
 * order for branches listed in metadata of signed commit.
 *
 * See T7421 for details.
 *
 * Returns: @TRUE if refs are correctly set
 */
gboolean
aum_ostree_upgrader_set_refs(AumOstreeUpgrader *self)
{
  g_autoptr (OstreeRepo) repo = NULL;
  g_autoptr (GVariant) commit = NULL;
  g_autoptr(GVariant) metadata = NULL;
  g_autoptr (GError) error = NULL;
  g_auto (GStrv) refs = NULL;
  g_autofree char *collection_id = NULL;
  g_autofree char *booted_ref = NULL;
  g_autofree char *target_ref = NULL;
  OstreeDeployment *booted_deployment = NULL;
  const gchar *booted_csum = NULL;
  GKeyFile *origin_conf_r = NULL;
  GKeyFile *origin_conf_w = NULL;
  g_autofree char *origin_refspec = NULL;
  g_autofree char *target_refspec = NULL;
  g_autofree char *origin_remote = NULL;
  g_autofree char *origin_ref = NULL;


  g_return_val_if_fail(self, FALSE);

  if (!ostree_sysroot_get_repo (self->sysroot, &repo, NULL, &error))
    {
      g_warning("Unable to get repo: %s", error->message);
      return FALSE;
    }

  booted_deployment = ostree_sysroot_get_booted_deployment (self->sysroot);
  if (booted_deployment == NULL)
    {
      g_warning ("Unable to detect booted deployment");
      return FALSE;
    }
  booted_csum = ostree_deployment_get_csum (booted_deployment);
  g_debug ("Booted commit ID: %s", booted_csum);

  if (!ostree_repo_load_variant (repo,
                                 OSTREE_OBJECT_TYPE_COMMIT,
                                 booted_csum,
                                 &commit,
                                 &error))
    {
      g_warning ("Unable to get commit info: %s", error->message);
      return FALSE;
    }

  metadata = g_variant_get_child_value (commit, 0);

  if (!aum_get_metadata_collection_refs (metadata,
                                         &collection_id,
                                         &refs,
                                         &error))
    {
      g_warning ("Unable to get collection ID refs: %s", error->message);
      return FALSE;
    }

  if (!aum_validate_collection_refs (repo,
                                     collection_id,
                                     refs,
                                     &booted_ref,
                                     &error))
    {
      g_warning ("Unable to get ref for %s", collection_id);
      return FALSE;
    }

  target_ref = aum_get_target_ref(refs);

  if (!ostree_validate_rev (target_ref, &error))
    {
      g_warning ("Invalid ref name: %s", target_ref);
      return FALSE;
    }

  if (g_strcmp0 (booted_ref, target_ref))
    {
      const OstreeCollectionRef ref_new = { (gchar *) collection_id, (gchar *) target_ref };
      const OstreeCollectionRef ref_del = { (gchar *) collection_id, (gchar *) booted_ref };

      if (!ostree_repo_prepare_transaction (repo, NULL, NULL, &error))
        {
          g_warning("Problem with transaction preparation: %s", error->message);
          return FALSE;
        }

      /* Set a new collection ID ref */
      ostree_repo_transaction_set_collection_ref (repo, &ref_new, booted_csum);
      /* Remove a previous collection ID ref */
      ostree_repo_transaction_set_collection_ref (repo, &ref_del, NULL);

      /* Set a new branch for origin */
      ostree_repo_transaction_set_ref (repo, "origin", target_ref, booted_csum);
      /* Remove a previous branch from origin */
      ostree_repo_transaction_set_ref (repo, "origin", booted_ref, NULL);

      /* Set a new branch for local ref */
      ostree_repo_transaction_set_ref (repo, NULL, target_ref, booted_csum);
      /* Remove a previous branch from local */
      ostree_repo_transaction_set_ref (repo, NULL, booted_ref, NULL);

      if (!ostree_repo_commit_transaction (repo, NULL, NULL, &error))
        {
          g_warning("Unable to reset the default branch: %s", error->message);
          return FALSE;
        }
    }

  /* Additionally need to change origin for the deployment.
   * It is inherited from the previous deployment in case of offline update.
   * Need to reset the target branch to correct version for deployment origin as well.
   */
  origin_conf_r = ostree_deployment_get_origin (booted_deployment);
  origin_refspec = g_key_file_get_string (origin_conf_r, "origin", "refspec", NULL);

  g_debug("Deployment origin refspec: %s", origin_refspec);
  if (!ostree_parse_refspec (origin_refspec, &origin_remote, &origin_ref, &error))
    {
      g_warning("Unable to parse deployment origin: %s", error->message);
      return FALSE;
    }

  if (g_strcmp0 (origin_ref, target_ref))
    {
      target_refspec = g_strconcat (origin_remote, ":", target_ref, NULL);
      origin_conf_w = ostree_sysroot_origin_new_from_refspec (self->sysroot, target_refspec);


      if (!ostree_sysroot_write_origin_file (self->sysroot,
                                             booted_deployment,
                                             origin_conf_w,
                                             NULL, &error))
        {
          g_warning("Unable to update deployment origin: %s", error->message);
          return FALSE;
        }

      g_debug("Updated origin refspec: %s", target_refspec);
    }

  g_message ("Default branch is 'origin:%s'", target_ref);
  return TRUE;
}

G_DEFINE_TYPE (AumOstreeUpgrader, aum_ostree_upgrader, G_TYPE_OBJECT)

void
set_system_upgrade_error(aumApertisUpdateManager *aum_dbus,
                         AumOstreeUpgradeError error,
                         gchar *description)
{
  GVariant *value;

  value = g_variant_new("(us)", error, description);
  aum_apertis_update_manager_set_system_upgrade_error (aum_dbus, value);
}
