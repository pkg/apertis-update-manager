/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_OSTREE_UPGRADER_H__
#define __AUM_OSTREE_UPGRADER_H__

#include <glib-object.h>
#include <glib.h>
#include <gio/gio.h>
#include "apertis-update-manager-dbus.h"

G_BEGIN_DECLS

typedef enum {
        AUM_OSTREE_UPGRADE_STATE_UNKNOWN = 0,
        AUM_OSTREE_UPGRADE_STATE_CHECKING,
        AUM_OSTREE_UPGRADE_STATE_DOWNLOADING,
        AUM_OSTREE_UPGRADE_STATE_DEPLOYING,
        AUM_OSTREE_UPGRADE_STATE_DECRYPTING,
        AUM_OSTREE_UPGRADE_STATE_AVAILABLE,
        AUM_OSTREE_UPGRADE_STATE_PENDING = 100,
        AUM_OSTREE_UPGRADE_STATE_UPTODATE = 200
} AumOstreeUpgradeState;

typedef enum {
       AUM_NO_ERROR = 0,
       /* Downloading error from 100 */
       /* Deploying error from 200 */
       /* Decrypting error from 300 */
       AUM_ERROR_UNABLE_TO_UNLOCK = 300
       /* Available error from 400 */
       /* Update pending error from 500 */
} AumOstreeUpgradeError;

void set_system_upgrade_error(aumApertisUpdateManager *aum_dbus,
			      AumOstreeUpgradeError error,
			      gchar *description);

/*
 * Type declaration.
 */
#define AUM_TYPE_OSTREE_UPGRADER aum_ostree_upgrader_get_type ()
G_DECLARE_FINAL_TYPE (AumOstreeUpgrader,
                      aum_ostree_upgrader,
                      AUM,
                      OSTREE_UPGRADER,
                      GObject)

void aum_ostree_upgrader_set_active(AumOstreeUpgrader *self,
                                      gboolean active,
                                      guint poll_seconds);

void
aum_ostree_upgrader_set_ready (AumOstreeUpgrader *self);

void aum_ostree_upgrader_set_dry_run (AumOstreeUpgrader *self, gboolean mode);

void aum_ostree_upgrade_apply_pending(AumOstreeUpgrader *self);

gboolean aum_ostree_upgrader_undeploy (AumOstreeUpgrader *self);

void
aum_ostree_upgrade_set_state(AumOstreeUpgrader *self,
                            AumOstreeUpgradeState state);

gboolean aum_ostree_upgrade_apply_static_delta(AumOstreeUpgrader *self,
                                               const gchar *filename);

gboolean aum_ostree_upgrader_static_delta_is_running (AumOstreeUpgrader *self);

void aum_ostree_upgrader_static_delta_cancel (AumOstreeUpgrader *self);

gboolean
aum_ostree_upgrader_check_blacklisted(const gchar *filename,
                                      const gchar *checksum);
void
aum_ostree_upgrader_set_blacklisted(const gchar *filename,
                                    const gchar *checksum);

gboolean
aum_ostree_upgrader_set_refs(AumOstreeUpgrader *self);
G_END_DECLS

#endif /* __AUM_OSTREE_UPGRADER_H__ */
